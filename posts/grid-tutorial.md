---
title: How to Use Grid Layout
author: Lasya Pullakhandam
date: 2020-09-04
tags: tutorial, css, design, howto
---

The CSS grid is a two-dimensional layout system that allows us to stack and line up content in columns and rows. One-dimensional Flexbox elements can be used for smaller components in the grid layout. And since it is built into CSS, the native properties allow for better performance.

This post will go through the basics of CSS grid, how to set up a simple grid layout, and how we used it to clean up our own website.

<!-- more -->

### Components of a Grid Layout

<img src="/grid-layout.png" alt="grid components" width=600 />

### How to Set up a Grid Layout
   
1. Set up grid in HTML.

    ```
    <div class="grid-column-layout">
        <div class="one box">one</div>
        <div class="two box">two</div>
        <div class="three box">three</div>
        <div class="four box">four</div>
    </div>
    ```

2. Declare a grid container with columns and rows. You can use any measurement unit for this (fr, vh, vw, px).

    ```
    .grid-column-layout {
        display: grid;
        grid-template-columns: 1fr 1fr;
        grid-template-rows: 50vh 50vh;
    }
    ```
    
* Note: Extra styling of grid item.

    ```
    .box {
        padding: 5em;
        text-align: center;
        border: solid black;
    }
    ```

    <img src="/basic-layout.png" alt="basic grid layout" width=600 />

3. With multiple rows and columns you can create different content areas. Through the grid-row and grid-column properties you can 

    ```
    .grid-column-layout {
        display: grid;
        grid-template-columns: 1fr 2fr 1fr;
        grid-template-rows: 50vh 50vh; 
    }

    .one{
        grid-row: 1 / 3;
        grid-column: 1 / 2;
    }

    .two{
        grid-row: 1 / 3;
        grid-column: 2 / 3;
    }

    .three{
        grid-row: 1 / 2;
        grid-column: 3 / 4;
    }

    .four{
        grid-row: 2 / 3;
        grid-column: 3 / 4;
    }

    ```

    <img src="/layout-2.png" alt="a little more complicated grid layout" width=600 />

<br>

### Applying Grid to Clean Up Our Own Website

Our staff page before was using various css selectors and `<br>` statements in the markdown file to manually space each time a staff member was added to the page. If the page was minimized, the photos and description would begin to overlap. Since this method of editing of staff page was inefficient, we changed to grid layout.

With grid layout, the grid container makes sure each grid item contains the information for one staff member. To add another person, all you need to do is add the text and image to another div inside the grid container.

How our website was displaying our staff page before: 
<br>
<img src="/old-layout.png" alt="old layout" />

How it is displaying it now: 
<br>
<img src="/new-layout.png" alt="new layout" />

