---
title: React Native Workshop in partnership with Level The Playing Field
author: Jeff Terrell
date: 2020-10-02
tags: events, react native
---

In participation with Level The Playing Field, I'll be teaching a series of workshops on React Native in the [Virtual App Lab](https://unc.zoom.us/j/785474464).

<!-- more -->

The first workshop will be at 3-4pm on Tuesday, October 13th. It will be fairly informal/casual. I don't intend to prepare much, if any, for it, but it will hopefully be a friendly way to learn a new technology.

The workshop is open to anybody who's interested.

Future workshops will probably be at the same time later this semester; stay tuned to [our #general Slack channel](https://join.slack.com/t/unc-app-lab/signup) for updates.

You can find more about [Level The Playing Field](http://leveltheplayingfieldgroup.com/) on their web site. Here's their mission statement:

> Level the Playing Field seeks to provide generational change for minority students by providing students at Historically Black Colleges and Universities (HBCU’s) and neighboring Primarily White Institutions (PWI’s) with the network and professional skills to form successful lives for themselves and their families through a scalable, three phase program.
