---
title: How to Build Your Own API  
author: Felipe Yanaga
date: 2021-11-15
tags: introduction, rest-api, programming, howto, resources
---

You are programmer that is sick of only doing class assignments and want to dabble with something that is closer to the real world? Well, this is the place for you my friend. This blog contains a video where we will go over both the theory and the practice of building your own API!  

<!--more-->

### Video containing the presentation

 - [Rest API Workshop Video](https://youtu.be/eWeuGM7FA8Q)

## Additional Resources 

If you want to learn more, here are some resources:

 - [Quarkus Rest Easy Tutorial Write-Up](https://quarkus.io/guides/rest-json)

 - [Quarkus Hibernate Panache Write-Up](https://quarkus.io/guides/hibernate-orm-panache)
 - [Quarkus Command Line Tutorial](https://quarkus.io/guides/cli-tooling)
