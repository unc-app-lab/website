---
title: The UNC Design Lab
author: Jeff Terrell
date: 2019-04-02
tags: design, resources
---

I recently discovered the [UNC Design
Lab](https://library.unc.edu/house/designlab/) and thought others should know
about it.

<!--more-->

It's in the (House) Undergraduate Library. Similar to the App Lab, you can drop
in with design questions, whether you're not sure how to get started, or you'd
like help with an existing design project. You can also make an appointment to
have somebody skilled in UI design review a design you've created.

The Design Lab also hosts [SkillfUL tech
workshops](http://skillful.web.unc.edu/) on design-related topics, where they
teach people how to use design-related tools like Adobe XD and other tools in
the Adobe Creative Cloud. They're done with workshops for the semester, but
they will have several dozen next semester, so check their website then for
details.

In general, this is a great resource for learning and improving your design
skills. If you're interested, check it out!
