---
title: How to build an app: an overview
author: Jeff Terrell
date: 2019-01-31
tags: web, mobile, introduction, design
---

I gave a talk last fall titled &lsquo;How to Build Your Killer App&rsquo; as part of Global Entrepreneurship Week. It was an overview of the entire process of building an app, intended for non-technical people who aren't necessarily interested in learning how to code. I published [my notes from the talk](http://terrell.web.unc.edu/files/2018/11/slides.pdf), which are a good read to understand the whole process and get pointers to additional resources.

<!--more-->

Here's the description I wrote for the talk:

> If you’re working to develop an entrepreneurial idea or venture, you have to keep the modern mode of life in mind: we’re online, on the go, all the time. That means that the ability to build a mobile or web app has become critical to supporting your entrepreneurial endeavors. Come hear Jeff Terrell, a UNC-Chapel Hill alumnus, entrepreneur and computer science professor of the practice, walk you through a beginner’s guide on how to build an app, including:
>
> - The lifecycle of an app.
> - Designing an app.
> - The parts of an app.
> - Choosing a technology to use.
> - Finding and working with developers.

If you want to read more, check out [my notes](http://terrell.web.unc.edu/files/2018/11/slides.pdf).
