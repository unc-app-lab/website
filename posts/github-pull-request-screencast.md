---
title: Creating a GitHub Pull Request (video)
author: Jeff Terrell
date: 2019-03-21
tags: git, tutorial, video
---

I just uploaded a video showing how to create a fork and a pull request in
GitHub. This is a little more complicated than pull requests within the same
repository because a repository owned by somebody else typically doesn't allow
you to push commits directly to it. Check out [the 9-minute
video](https://youtu.be/dUZ0bpGgVf4), or read on for details.

<!--more-->

The project I'm contributing to is [the build image for
Netlify](https://github.com/netlify/build-image). This is the Dockerfile that
specifies what software will be installed when the Netlify servers build a
static site from a repository. This enables a really nice "continuous
deployment" scenario: since I've connected Netlify to [my GitLab
repository](https://gitlab.com/unc-app-lab/website), I can tell Netlify to
automatically deploy any commit that gets pushed to the master branch. I just
have to tell Netlify how to build the site.

At the moment, I have to build the site myself and include the built site in
the repository. Then I just tell Netlify to deploy whatever it finds in the
`dist/` directory of the commit. This isn't great because anybody who
contributes a commit to the repository needs either to have discipline and
remember to build the site every time or else to install a `pre-commit` hook in
their local copy of the repository.

The reason I can't let Netlify build the site for me, like I do for [my
personal website](https://github.com/kyptin/jeffterrell.tech), is that [the
static site generator I'm using](https://gitlab.com/unc-app-lab/clj-ssg/) uses
the [Clojure CLI build tools](https://clojure.org/guides/deps_and_cli), which
aren't installed in the Netlify build image.

Which brings us back to the pull request. I cloned the [`netlify/build-image`
repository](https://github.com/netlify/build-image), added the Clojure CLI
tools and tested everything locally, drafted a commit message, then started
recording. During the recording, here's what I did:

- created a "fork" of the repository on the GitHub site, which clones the repository such that I own the copy (and can commit to it)
- renamed the `origin` remote, which was pointing to the main repository, to be called `upstream` (a conventional name)
- created a new `origin` remote to point to my fork
- set the upstream tracking branch to use the `origin` remote rather than the `upstream` remote
- created a commit
- pushed the commit to `origin`
- created a pull request from `kyptin/build-image` (my fork) to `netlify/build-image` (the main repo)

Again, here's [the 9-minute video on YouTube](https://youtu.be/dUZ0bpGgVf4).

For more information, you can [read about forks and pull requests in the GitHub
documentation](https://help.github.com/en/articles/creating-a-pull-request-from-a-fork).
