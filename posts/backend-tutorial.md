---
title: A backend tutorial
author: Jeff Terrell
date: 2019-02-15
tags: tutorial, backend, javascript, express.js, introduction
---

A few days ago, I gave a talk to participants of the [2019 UNC
Makeathon](https://www.makeathon.unc.edu/) about how to create a backend web
service.

<!--more-->

What is a backend web service? In a nutshell, it's where all the data lives in
a web app or mobile app. Some data can live on the device itself (like cookies
to keep track of your session), but most data needs to live in a centralized,
authoritative "source of truth". This source of truth is the backend. It
consists of a database (written by somebody else and used by you, the
programmer) and a server (written by you) that sits between the clients and the
database. The server receives requests by clients, validates them, and
retrieves and stores data in the database.

Anyway, it's a pretty essential part of most apps these days. You can learn
more about it from these resources from the talk:

- A [recording on YouTube](https://www.youtube.com/watch?v=ymJvejN9_ow)
- The [slides from the talk](https://gitlab.com/unc-app-lab/backend-tutorial/blob/master/talk-slides.pdf)
- The [tutorial source code](https://gitlab.com/unc-app-lab/backend-tutorial), which includes a simple example backend written in Javascript using [the Express.js framework](https://expressjs.com/).

If you're building a web app or mobile app and need help creating a backend,
feel free to swing by the App Lab [when it's open](/calendar/).
