---
title: A few resources for learning git
author: Jeff Terrell
date: 2019-02-04
tags: git, resources, tutorial
---

This morning I gave a lecture about git to my [COMP 523](http://www.cs.unc.edu/~stotts/COMP523-s19/) (software engineering laboratory) class. _(Update 2019-10-03: I just published [a video tutorial](https://youtu.be/UAWgWvERwew) of the same content.)_  Some know it well; others barely know what it is. Here are a few resources I've collected on learning git, with some notes about how appropriate they are to beginners vs. more experienced people.

<!--more-->

_(Edited 2019-03-25 to include the _Write Yourself a Git_ item as #6 below.)_

1. [Learn Git Branching (a tutorial game)](https://learngitbranching.js.org/)

   This is a web app teaching git one level at a time. It's more visual and interactive than most other approaches, and I think it explains concepts well and in a reasonable order. This might be my top recommendation for beginners. It's not as convenient for more experienced users, but they can scan the list of levels (on both the "main" and "remote" tabs) to see if there's a topic they don't know.

2. [RoleModel Best Practices](https://github.com/RoleModel/BestPractices/) (only the "Git" section)

   Not long after I joined [RoleModel Software](https://www.rolemodelsoftware.com/), I had the opportunity to overhaul their best practices around git. I've written much of the content there in an attempt to concisely explain how to use git in a way that's both effective for delivering value to your customer and friendly to your fellow developers. These documents are less about what git is or how it works and more about tasteful ways of using it, so they are probably more appropriate for those with some experience with git. Included are links to pages on [good error messages](http://www.rakeroutes.com/blog/deliberate-git/) ([honorable mention](https://chris.beams.io/posts/git-commit/)) and "[flight rules](https://github.com/k88hudson/git-flight-rules)" to guide git users when things go wrong.

3. [Pro Git, 2nd Edition](https://git-scm.com/book/en/v2), a book by Scott Chacon and Ben Straub.

   This is a well-written, thorough treatment of git from top to bottom. I think some of the illustrations are still etched on my brain. If you have the time to pursue mastery, I recommend a read. I expect it will be accessible both to beginners and to more experienced users. Oh, and it's free to read online.

4. [Git from the Bottom Up](https://jwiegley.github.io/git-from-the-bottom-up/), an online minibook by John Wiegley

   I haven't read this so can't honestly recommend it, but I like the idea of teaching git starting with the fundamental concepts. This is exactly how I taught it to my software engineering class. One reason I like this approach is that it can serve both novices and proficient users. The proficient users often have a vague concept of the fundamentals, but getting precision at this level can lead to mastery. Novices can learn the concepts without getting lost in a "recipe soup" of instructions. The commands and recipes make much more sense when the underlying concepts are understood first. If you have experience with this resource, I'd be interested to hear how well it served you.

5. [Magit](https://magit.vc/), a git porcelain inside Emacs

   Finally, I would be remiss not to mention Magit. People widely consider it to be one of the two "killer apps" for Emacs that entice users to switch their main editor (the other being [Org Mode](http://orgmode.org/), which I'm also a fan of). Magit is a keyboard-driven UI for using git. Most git commands are just a couple of keystrokes away. The interface is highly discoverable, and it regularly moves me to delight (seriously). Once you get the hang of it, you quickly start flying around, exerting your will into the repo in an almost frictionless way. It's really great.

6. [Write Yourself a Git](https://wyag.thb.lt/), by Thibault Polge

   _(Added 2019-03-25:)_ This is similar to the [Git from the Bottom Up](https://jwiegley.github.io/git-from-the-bottom-up/) minibook above, except that it's not just teaching the low-level concepts but showing how you might code your own simple version of git. It turns out that a working version of git, without all of the fancy tools git provides, but powerful and correct enough to create a commit that the real git understands, only takes 503 lines of Python. If you want to really understand git, I suspect that this is the best way to do it!
