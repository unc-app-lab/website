---
title: Where to Start in App Development for Non-Coders
author: Jonah Soberano
date: 2021-02-22
tags: introduction, figma, design, howto, resources
---

If you don't have experience with coding, you may be unsure on where to start with your web or mobile app ideas. Below are some tips that can help get you started.

<!--more-->

### Design Your App

When you have an app idea, it is best to start with some sort of design (e.g., Figma, Adobe XD, powerpoint, etc.) because it:

 - Forces you to clarify your thinking about what you're building
 - Can help convey to other people what their idea is; including developers who might want to get involved.
 - Can help convey your idea to your target audience.

### Additional Resources

Additionally, here are some other resources at your disposal:

 - Undergraduate library has a [design lab](https://library.unc.edu/house/designlab/) to help with design; they are more expert in that field than us. However, because they of remote learning they are currently closed so feel free to come to our office hours for design tips.
 - [Figma tutorial](/posts/2019/09/10/design-tutorial-using-figma/) in AppLab resources
 - Crash Course design books: "Non Designer's Design Book" by Robin Williams and "Don't Make Me Think" by Steve Krug

### Next Steps: Getting People Involved

When you have a design and you're ready to get other people involved:

 - Come into office hours and we can add to the [project ideas Trello board](https://trello.com/b/adrdTBg7/app-lab-ideas-board)
 - You can post to random channel on [slack](https://join.slack.com/t/unc-app-lab/signup)
 - You can network with CS clubs
 - You could ask [Stephanie Johnson](https://cs.unc.edu/people/stephanie-johnson/) about Careers/Bits and Bytes newsletter

As always, feel free to swing by the App Lab [when it's open](/calendar/) if you have any questions. We look forward to seeing what you create!
