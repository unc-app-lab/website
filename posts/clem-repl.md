---
title: Integrating Clem with Cursive
author: Daniel Manila
date: 2020-01-17
tags: clem, clojure, tutorial
---

[Clem](https://gitlab.com/unc-app-lab/clem) is a program intended to make
development with Clojure easier by helping explain Clojure's sometimes cryptic
error messages. It integrates with your REPL an uses an online database to
attempt to find more human-friendly messages to explain your errors.

This post will go through the steps required to integrate Clem with [Cursive](https://applab.unc.edu/posts/2019/09/27/installing-cursive-ide-for-clojure/).

<!-- more -->

This post assumes you have Cursive already installed (if not, follow [this
tutorial](https://applab.unc.edu/posts/2019/09/27/installing-cursive-ide-for-clojure/)).

1. Create a new deps.edn Clojure project

<img src="/cursive-proj.png" alt="Select project type" />

2. Choose a name for the project. For this example, will use `clem-proj`.
3. Edit the `deps.edn` file so that it looks like this:

    `{:deps {clem-repl {:mvn/version "0.1.2-SNAPSHOT"}}}`

4. Right click the top-level project directory and select _New -> Directory_,
   and name it `src`
5. Right click the `src` folder and select _New -> Clojure Namespace_
6. Give the namespace a name like `clem-proj.core`. Remember this namespace,
   we'll need it later.
7. Right click the top-level project directory and select _New -> File_, name
   the file `.clem-repl.edn`. The leading period is important.
8. Edit `.clem-repl.edn` and include the following map:

    ```
    {:host-domain "http://clem.applab.unc.edu"
     :project-namespace 'clem-proj}
    ```

    The hostname is the URL to the server which hosts your clem database. This
    URL is the server hosted by the AppLab.

    The project-namespace is the root level prefix for your project. In this
    case it is simple `clem-proj` (the first part of `clem-proj.core`).
9. Right click the top-level project directory and select _New -> File_, name
   the file `.nrepl.edn`. The leading period is important.
10. Edit `.nrepl.edn` and include the following map:

    ```
    {:middleware [edu.unc.applab.clem-repl.nrepl-middleware/clem-caught]}
    ```

11. Start a repl by right-clicking `deps.edn` and selecting _Run REPL for
    clem-proj_.
12. Code as normal. Errors will be caught by Clem and compared to the database.
