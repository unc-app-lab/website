---
title: The UNC App Lab
stem: index
---

The App Lab is where UNC students can learn how to build web and mobile
applications, run by UNC Computer Science Professor of the Practice [Dr. Jeff
Terrell](http://terrell.web.unc.edu/). It's in Room 027 of Sitterson Hall, and
it's open weekday afternoons. You can view [the full schedule](/calendar/). You
can learn more [about the App Lab](/about/).

Are you a student interested in participating in the App Lab and learning web
and mobile app development? You can learn [how to get
involved](/get-involved/).

Do you have a need for an app (or any other questions about software)? Come
to the App Lab and talk to our experienced staff.

_Update for spring, 2022:_ also check our our [App-a-thon
competition](../app-a-thon/)!
