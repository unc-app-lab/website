---
title: Calendar
stem: calendar
---
<div>
<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=1&amp;bgcolor=%23ffffff&amp;ctz=America%2FNew_York&amp;src=Y3MudW5jLmVkdV90azQyY3JoamM3dDY1MXFkdXJnMnNmMTk4b0Bncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;src=Y3MudW5jLmVkdV84cGNoaGhycGc0Nm92ajJkanF1aGI0cjI3MEBncm91cC5jYWxlbmRhci5nb29nbGUuY29t&amp;color=%230B8043&amp;color=%23EF6C00&amp;mode=WEEK&amp;showPrint=0&amp;showTabs=1&amp;showTz=0" style="border-width:0" width="650" height="600" frameborder="0" scrolling="no"></iframe></div>

The calendar above shows our current hours for the upcoming week. During those
hours we are open for any kind of help for any skill level. From debugging, to
planning an app, to learning a new language&mdash;we're here to help.

As of Monday, August 30, 2021, the physical App Lab in Sitterson Hall room
SN027 is open again!

Per UNC community standards, masks are required at all times.
