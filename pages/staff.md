---
title: Meet the App Lab Staff!
stem: staff
---

<div class="grid-column-layout">

<div class="box">
<img class="headshot" src="/jeff.jpg" alt="jeff"/>

### Jeff Terrell

- Computer Science Professor of the Practice

I hail from Texas and Arkansas, although I've been in this area of NC since I came here (to UNC-CS) for grad school in 2004. I've been a tech entrepreneur and "software craftsman" since I graduated in 2009, and I'm excited to share the software skills I've learned with you all in the App Lab. I have a lot of experience with web apps (and a little less with mobile apps), so I can help with many different kinds of things. In my spare time, I enjoy hanging out with my family (including my 3 young kiddos), reading (usually something Christian/spiritual or SF), watching movies, and occasionally playing StarCraft 2.
</div>

<div class="box">
<img class="headshot" src="/daniel.jpg" alt="daniel"/>

### Daniel Manila

- Senior
- Computer Science

I'm from Durham, North Carolina. I like reading books, playing games (especially strategy board games), and figuring out how to fix the newest problem with my Linux system. I enjoy solving new problems, so please throw any questions you have at me and I'll do my best to help.
</div>

<div class="box">
<img class="headshot" src="/nate.jpg" alt="nate"/>

### Nate Fulmer

- Senior
- Computer Science major, Statistics minor

I'm from Atlanta, Georgia and my hobbies include reading, swimming, and letting my little siblings beat me at UNO. Some projects I've worked on before include C# .NET automated test development, JavaScript Node.js coding to build a web data connector between a REST API and Tableau, and python machine learning stuff (PyTorch, TensorFlow, etc). I'm looking forward to helping people with their projects!
</div>

<div class="box">
<img class="headshot" src="/felipe.png" alt="felipe"/>

### Felipe Yanaga

- Sophomore
- Computer Science major
 
I am originally from Brazil - with Japanese descent, but I currently live in Cary, NC! I love to read and learn more about the things that I am interested in! Currently, those things are British literature and psychology! When it comes to CS, my favorite things to do are: developing life-changing apps and helping others build their own apps. Please come see me at the App Lab!  
</div>

<div class="box">
<img class="headshot" src="/lama.jpeg" alt="lama"/>

### Lama Abed

- Sophomore
- Computer Science major.

Hey guys! I am Lama, but I go by Lia. I'm from Palestine, I enjoy painting and drawing and making creative content online. I have experiences in web development with HTML, CSS, and other coding languages such as Python and Java. I really love coming up with creative and seemingly impossible ideas then try to conduct them, and I am excited to hear about your amazing projects and be a part of your coding journey!
</div>
</div>

## Volunteers
<div class="grid-column-layout">

<div class="box">
<img class="headshot" src="/bailey.jpg" alt="bailey"/>

### Bailey Van Wormer

What's up, my name is Bailey Van Wormer and I am from Cary, NC. I love all things mobile app development, particularly Swift and SwiftUI. I am happy to be volunteering in the App Lab for the Spring 2022 semester. If I'm not in the App Lab you can catch me at many of the UNC sporting events, producing electronic music, or working with my friends over at App Team Carolina, where I am the Chief Operating Officer. I look forward to making new friends in the App Lab this semester!
</div>


<div class="box">
<img class="headshot" src="/vitor.jpg" alt="vitor"/>

### Vitor Inserra

I am from Sao Paulo, Brazil. I have many hobbies, but my favorites are fencing,
playing piano and reading. When it comes to coding, my favorite language is
Python. I am really into backend development, but recently I created an interest
for frontend with React JS. I hope to help everyone at the lab!
</div>
</div>

<h1>Alumni</h1>
- Grant Miller
- [Brandon Shearin](https://www.linkedin.com/in/brandon-shearin/)
- [Manuel Cabrejos](https://www.linkedin.com/in/mcabrejost)
- Mary Luong
- [Anna Truelove](https://www.linkedin.com/in/anna-truelove/)
- [Jonah Soberano](https://www.linkedin.com/in/jonahsoberano/)
- [Isha Kabra](https://github.com/isha300)
- [Rohaid Bakheet](https://www.linkedin.com/in/rohaid-bakheet/)
- Lasya Pullakhandam
